#include "version.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include <limits.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg1");
	TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_avg(4, 2), "Error in integer_avg2");
	TEST_ASSERT_EQUAL_INT_MESSAGE(5, integer_avg(3, 7), "Error in integer_avg3");
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-1, 1), "Error in integer_avg4");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-3, integer_avg(-4, -2), "Error in integer_avg5");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-5, integer_avg(-3, -7), "Error in integer_avg6");
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-1, 1), "Error in integer_avg7");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-3, integer_avg(-9, 3), "Error in integer_avg8");
	TEST_ASSERT_EQUAL_INT_MESSAGE(5, integer_avg(-3, 13), "Error in integer_avg9");
	
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 1), "Error arrondi in integer_avg1");
	TEST_ASSERT_EQUAL_INT_MESSAGE(2, integer_avg(3, 2), "Error arrondi in integer_avg2");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-2, integer_avg(-7, 2), "Error arrondi in integer_avg3");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(-1, -2), "Error arrondi in integer_avg4");
	
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(INT_MAX, INT_MIN), "Error MAX/MIN in integer_avg1");
	TEST_ASSERT_EQUAL_INT_MESSAGE(INT_MAX, integer_avg(INT_MAX, INT_MAX), "Error MAX/MIN in integer_avg2");
	TEST_ASSERT_EQUAL_INT_MESSAGE(INT_MIN, integer_avg(INT_MIN, INT_MIN), "Error MAX/MIN in integer_avg3");
}